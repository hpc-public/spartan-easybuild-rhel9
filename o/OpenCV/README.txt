OpenCV was rebuilt with the following in the EB file
  - Disable default optarch values by setting "toolchainopts = {'optarch': False}"
  
  - Set Skylake AVX512 as baseline in configopts:  "-DCPU_BASELINE=AVX512_SKX",

Without these, the build would fail with error msg:
  Don't know how to configure OpenCV in accordance with --optarch='{'Intel': 'xCORE-AVX512', 'GCC': 'march=skylake-avx512'}'

