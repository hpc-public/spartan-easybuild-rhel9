name = 'OpenMPI'
version = '3.1.4'

homepage = 'https://www.open-mpi.org/'
description = """The Open MPI Project is an open source MPI-3 implementation."""

toolchain = {'name': 'GCC', 'version': '11.3.0'}

source_urls = ['https://www.open-mpi.org/software/ompi/v%(version_major_minor)s/downloads']
sources = [SOURCELOWER_TAR_GZ]
checksums = ['a7c34ad052ea8201ed9e7389994069fe6996403beabdd2d711caf0532808156c']

patches = ['OpenMPI-3.1.3-add_ompi_datatype_attribute_to_release_ucp_datatype.patch', 'ompi-hdr-ndr-3.1.patch']


#builddependencies = [
#    ('pkgconf', '1.8.0'),
#    ('Perl', '5.34.1'),
#    ('Autotools', '20220317'),
#]

dependencies = [
    ('zlib', '1.2.12'),
    ('hwloc', '1.11.8'),
    ('UCX', '1.13.1'),
    ('libfabric', '1.15.1'),
    ('UCC', '1.0.0'),
]

# Update configure to include changes from the "internal-cuda" patch
# by running a subset of autogen.pl sufficient to achieve this
# without doing the full, long-running regeneration.
preconfigopts = ' && '.join([
    'cd config',
    'autom4te --language=m4sh opal_get_version.m4sh -o opal_get_version.sh',
    'cd ..',
    'autoconf',
    'autoheader',
    'aclocal',
    'automake',
    ''
])

# CUDA related patches and custom configure option can be removed if CUDA support isn't wanted.
#configopts = '--with-cuda=internal '

# disable MPI1 compatibility for now, see what breaks...
# configopts += '--enable-mpi1-compatibility '
configopts = '--with-threads=posix --enable-shared --enable-mpi-thread-multiple --with-verbs '
configopts += '--enable-mpirun-prefix-by-default '  # suppress failure modes in relation to mpirun path
configopts += '--with-hwloc=$EBROOTHWLOC '  # hwloc support
configopts += '--with-ucx=$EBROOTUCX '  # Disable spurious detection of Cisco UCX
configopts += '--enable-mpi-cxx '  # Enable cxx


# to enable SLURM integration (site-specific)
# configopts += '--with-slurm --with-pmi=/usr/include/slurm --with-pmi-libdir=/usr'
configopts += '--with-slurm=/apps/slurm/latest --with-pmi=/apps/slurm/latest --with-pmi-libdir=/apps/slurm/latest/lib'

postinstallcmds = ["cp /apps/easybuild-2022/easybuild/local-easybuild/o/OpenMPI/templates/ompi-mca-params-%(version_major)s.conf %(installdir)s/etc/openmpi-mca-params.conf"]


moduleclass = 'mpi'
